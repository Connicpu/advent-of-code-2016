static PATTERNS: &'static [&'static str] = include!("../input/2.txt");
static CODES: [[char; 5]; 5] = [
    [' ', ' ', '1', ' ', ' '],
    [' ', '2', '3', '4', ' '],
    ['5', '6', '7', '8', '9'],
    [' ', 'A', 'B', 'C', ' '],
    [' ', ' ', 'D', ' ', ' '],
];

fn is_valid(x: i32, y: i32) -> bool {
    if x < 0 || y < 0 {
        return false;
    }
    if x > 4 || y > 4 {
        return false;
    }

    CODES[y as usize][x as usize] != ' '
}

fn main() {
    let mut x = 1;
    let mut y = 1;

    for line in PATTERNS {
        for c in line.chars() {
            match c {
                'U' => if is_valid(x, y - 1) {
                    y -= 1;
                },
                'D' => if is_valid(x, y + 1) {
                    y += 1;
                },
                'L' => if is_valid(x - 1, y) {
                    x -= 1;
                },
                'R' => if is_valid(x + 1, y) {
                    x += 1;
                },
                _ => {}
            }
        }

        print!("{}", CODES[y as usize][x as usize]);
    }
    println!("");
}
