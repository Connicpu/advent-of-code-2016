use Instruction::*;
use Direction::*;
use std::collections::HashSet;

#[derive(Copy, Clone)]
enum Instruction {
    L(i32),
    R(i32),
}

enum Direction {
    North,
    East,
    South,
    West,
}

struct State {
    visits: HashSet<(i32, i32)>,
    found: bool,    
    dir: Direction,
    x: i32,
    y: i32,
}

impl State {
    fn left(&mut self) -> &mut Self {
        self.dir = match self.dir {
            North => West,
            East => North,
            South => East,
            West => South,
        };
        self
    }

    fn right(&mut self) -> &mut Self {
        self.dir = match self.dir {
            North => East,
            East => South,
            South => West,
            West => North,
        };
        self
    }

    fn incr(&mut self) {
        match self.dir {
            North => self.y += 1,
            East => self.x += 1,
            South => self.y -= 1,
            West => self.x -= 1,
        }
    }

    fn walk(&mut self, n: i32) {
        for _ in 0..n {
            self.incr();
            self.mark();
        }
    }

    fn mark(&mut self) {
        if self.found {
            return;
        }

        let pos = (self.x, self.y);
        if !self.visits.insert(pos) {
            println!("Visited {:?} twice", pos);
            self.found = true;
        }
    }
}

fn main() {
    let instructions = include!("../input/1.txt");
    let mut state = State {
        visits: HashSet::new(),
        found: false,
        dir: North,
        x: 0,
        y: 0,
    };

    for &inst in instructions.iter() {
        match inst {
            L(n) => state.left().walk(n),
            R(n) => state.right().walk(n),
        }
    }

    println!("Blocks away: {}", state.x.abs() + state.y.abs());
}
