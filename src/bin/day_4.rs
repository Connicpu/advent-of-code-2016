#![feature(field_init_shorthand)]

use std::str::FromStr;
use std::cmp::Ordering;

static DATA: &'static str = include_str!("../input/4.txt");

#[derive(PartialEq, Eq)]
struct Count(i32, char);
const COUNTS_INIT: [Count; 26] = [
    Count(0, 'a'),
    Count(0, 'b'),
    Count(0, 'c'),
    Count(0, 'd'),
    Count(0, 'e'),
    Count(0, 'f'),
    Count(0, 'g'),
    Count(0, 'h'),
    Count(0, 'i'),
    Count(0, 'j'),
    Count(0, 'k'),
    Count(0, 'l'),
    Count(0, 'm'),
    Count(0, 'n'),
    Count(0, 'o'),
    Count(0, 'p'),
    Count(0, 'q'),
    Count(0, 'r'),
    Count(0, 's'),
    Count(0, 't'),
    Count(0, 'u'),
    Count(0, 'v'),
    Count(0, 'w'),
    Count(0, 'x'),
    Count(0, 'y'),
    Count(0, 'z'),
];

impl Ord for Count {
    fn cmp(&self, other: &Count) -> Ordering {
        match self.0.cmp(&other.0) {
            Ordering::Greater => Ordering::Less,
            Ordering::Less => Ordering::Greater,
            Ordering::Equal => self.1.cmp(&other.1),
        }
    }
}

impl PartialOrd for Count {
    fn partial_cmp(&self, other: &Count) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Copy, Clone, Debug)]
struct Room<'a> {
    name: &'a str,
    sector_id: i32,
    checksum: &'a str,
}

impl<'a> Room<'a> {
    fn parse(line: &'a str) -> Room<'a> {
        let i = line.find('[').unwrap();
        let checksum = &line[i + 1..i + 6];
        let name = &line[..i];
        let i = name.rfind('-').unwrap();
        let sector_id = i32::from_str(&name[i + 1..]).unwrap();
        let name = &name[..i];
        Room { name, sector_id, checksum }
    }

    fn calc_checksum(&self) -> [char; 5] {
        let mut counts = COUNTS_INIT;
        let a = b'a';

        for c in self.name.chars() {
            match c {
                'a'...'z' => {
                    let i = (c as u8 - a) as usize;
                    counts[i].0 += 1;
                }
                _ => {}
            }
        }

        counts.sort();

        [
            counts[0].1,
            counts[1].1,
            counts[2].1,
            counts[3].1,
            counts[4].1,
        ]
    }

    fn real(&self) -> bool {
        self.calc_checksum().iter().cloned().eq(self.checksum.chars())
    }

    fn decrypt(&self, c: char) -> char {
        let a = b'a';

        match c {
            'a'...'z' => {
                ((((c as u8 - a) as i32 + self.sector_id) % 26) as u8 + a) as char
            }
            _ => ' ',
        }
    }
}

// cargo run --bin day_4 | grep "north"
fn main() {
    let room = Room::parse("aaaaa-bbb-z-y-x-123[abxyz]");
    println!("{:?}", room);
    println!("{:?}", room.calc_checksum());

    let mut total = 0;
    for room in DATA.lines() {
        let room = Room::parse(room);
        
        if room.real() {
            total += room.sector_id;
            for c in room.name.chars() {
                print!("{}", room.decrypt(c));
            }
            println!(": {}", room.sector_id);
        }
    }
    println!("total: {}", total);
}
