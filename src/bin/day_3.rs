#![feature(conservative_impl_trait)]

struct Triangle(i32, i32, i32);

static TRIANGLES: &'static [Triangle] = include!("../input/3.txt");

impl Triangle {
    fn largest(&self) -> i32 {
        if self.0 > self.1 && self.0 > self.2 {
            0
        } else if self.1 > self.2 {
            1
        } else {
            2
        }
    }

    fn possible(&self) -> bool {
        match self.largest() {
            0 => self.1 + self.2 > self.0,
            1 => self.0 + self.2 > self.1,
            _ => self.0 + self.1 > self.2,
        }
    }

    fn rotate(chunk: &[Triangle]) -> impl Iterator<Item=Triangle> {
        Some(Triangle(chunk[0].0, chunk[1].0, chunk[2].0)).into_iter()
            .chain(Some(Triangle(chunk[0].1, chunk[1].1, chunk[2].1)))
            .chain(Some(Triangle(chunk[0].2, chunk[1].2, chunk[2].2)))
    }
}

fn main() {
    println!("Possible 1: {}", TRIANGLES.iter().filter(|t| t.possible()).count());
    println!("Possible 2: {}", TRIANGLES.chunks(3).flat_map(Triangle::rotate).filter(|t| t.possible()).count());
}
