extern crate md5;

use std::iter::FromIterator;

fn good(b: [u8; 16]) -> Option<(usize, char)> {
    if b[0] == 0 && b[1] == 0 && b[2] & 0xF0 == 0 {
        let p = match b[2] & 0xF {
            b@0...7 => b as usize,
            _ => return None,
        };
        let c = match (b[3] & 0xF0) >> 4 {
            b@0...9 => b'0' + b,
            b@10...15 => b'a' + (b - 10),
            _ => unreachable!(),
        } as char;
        Some((p, c))
    } else {
        None
    }
}

fn main() {
    let mut result = [' '; 8];
    let input = "cxdnnyjw";
    let mut count = 0;
    for i in 0.. {
        let key = format!("{}{}", input, i);
        if let Some((p, c)) = good(md5::compute(key.as_bytes())) {
            if result[p] == ' ' {
                count += 1;
                result[p] = c;
            }

            if count == 8 {
                break;
            }
        }
    }

    println!("{}", String::from_iter(result.iter().cloned()));
}