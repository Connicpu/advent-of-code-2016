#![feature(inclusive_range_syntax)]

static DATA: &'static str = include_str!("../input/6.txt");

fn main() {
    let mut counts = [[(0i32, ' '); 26]; 8];
    for pos in counts.iter_mut() {
        for (p, c) in pos.iter_mut().zip(b'a'...b'z') {
            p.1 = c as char;
        }
    }

    for line in DATA.lines() {
        for (pos, c) in line.chars().enumerate() {
            let idx = (c as u8 - b'a') as usize;
            // Flip this to `-=` for part 1
            counts[pos][idx].0 += 1;
        }
    }

    for pos in counts.iter_mut() {
        pos.sort();
        print!("{}", pos[0].1);
    }
    println!("");

}

